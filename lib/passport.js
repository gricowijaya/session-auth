const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { User } = require('../db/models')

// handler to authenticate the user 
const authenticate = async (username, password, done) => { 
    try { 
        // call the method of authenticate from the User Models
        const user = await User.authenticate({username, password});
        return done(null, user);
    } catch(err) { 
        // this code below will be thrown into flash
        return done(null, false, { message: err.message })
    }
}

// create the local strategy authentication
passport.use(
    new LocalStrategy({ usernameField: 'username', passwordField: 'password' }, authenticate)
);

// in the below code is to create  the user session
passport.serializeUser(
    (user, done) => done(null, user.id)
)
// in the below code is to remove the user session
passport.deserializeUser(
    async (id, done) => done(null, await User.findByPk(id))
)

// another way to create the deserializeUser method
// passport.deserializeUser(
//     async (user, done) => done(null, user)
// )

module.exports = passport
